package com.example.davaleba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class SecondActivity : AppCompatActivity() {

    private lateinit var ageEditText: EditText
    private lateinit var finishButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        ageEditText = findViewById(R.id.editTextAge)
        finishButton = findViewById(R.id.buttonFinish)

        var name = ""

        if(intent.extras !=null) {
            name = intent.extras?.getString("NAME", "").toString()
        }

        finishButton.setOnClickListener{
            val age = ageEditText.text.toString().toInt()

            val intent = Intent(this, FinishActivity::class.java)
            intent.putExtra("AGE", age)
            intent.putExtra("NAME", name)
            startActivity(intent)
            finish()
        }
    }
}