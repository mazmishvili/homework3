package com.example.davaleba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    private lateinit var nameEditText: EditText
    private lateinit var nextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("MyData", "onCreate")

        nameEditText = findViewById(R.id.editTextName)
        nextButton = findViewById(R.id.buttonNext)

        nextButton.setOnClickListener{
            val name = nameEditText.toString()

            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("NAME", name)
            startActivity(intent)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()

        Log.d("MyData", "onStart")

    }

    override fun onResume() {
        super.onResume()

        Log.d("MyData", "onResume")
    }

    override fun onPause() {
        super.onPause()

        Log.d("MyData", "onPause")
    }

    override fun onStop() {
        super.onStop()

        Log.d("MyData", "onStop")
    }

    override fun onRestart() {
        super.onRestart()

        Log.d("MyData", "onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d("MyData", "onDestroy")
    }
}