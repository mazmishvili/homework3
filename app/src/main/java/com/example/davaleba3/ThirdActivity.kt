package com.example.davaleba3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class ThirdActivity : AppCompatActivity() {

    private lateinit var surname: EditText
    private lateinit var nextbutton: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        surname = findViewById(R.id.surname)
        nextbutton = findViewById(R.id.nextbutton)

        var name = ""
        var age: Int

        if (intent.extras != null) {
            name = intent.extras?.getString("name").toString()

            age = intent.extras?.getInt("age").toString().toInt()
        }

        nextbutton.setOnClickListener{
            var gvari = surname.text.toString()

            val intent = Intent(this,FinishActivity::class.java)
            intent.putExtra("gvari", gvari)
            startActivity(intent)
        }

    }
}